# Petra Ride Android Task

Implementation of the Petra Ride task for Android platform.


## Technologies and architecture

 - MVVM
 - Material Design
 - Repository Pattern
 - Room DB
 - Coroutines
 - Dependency Injection with Hilt
 - LiveData
 - Diff Utils
 - Data Binding
 - Navigation Component
 - JUnit 4
 - Mockito

## Extras

- Edit Item
- Delete Item
