package com.petraride.testapp.ui.additem

import android.database.sqlite.SQLiteConstraintException
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.petraride.testapp.db.entity.Item
import com.petraride.testapp.repository.ItemsRepository
import com.petraride.testapp.util.DateUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import com.petraride.testapp.R
import com.petraride.testapp.nav.NavigationCommand
import com.petraride.testapp.ui.entity.ItemMapper
import com.petraride.testapp.ui.entity.UiItem
import org.junit.Rule

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.*

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class AddItemViewModelTest {

    @Mock
    private lateinit var repository: ItemsRepository
    private lateinit var viewModel: AddItemViewModel

    @get:Rule
    var instantExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        viewModel = AddItemViewModel(repository)
        Dispatchers.setMain(UnconfinedTestDispatcher(TestCoroutineScheduler()))
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun onTextChanged_add_mode() {
        //valid form
        enterValidFields()
        assertEquals(R.string.add_item_title, viewModel.appBarTitleResource.value)
        assertTrue(viewModel.enableSaveButton.value!!)

        //invalid form
        viewModel.uiItem.count = "inv"
        viewModel.onTextChanged()
        assertFalse(viewModel.enableSaveButton.value!!)
    }

    @Test
    fun onTextChanged_edit_mode() {
        //item to edit
        initEditMode()
        //title
        assertEquals(R.string.edit_item_title, viewModel.appBarTitleResource.value)
        //disable id text
        assertFalse(viewModel.enableIdField.value!!)

        //No value changed
        assertFalse(viewModel.enableSaveButton.value!!)

        //invalid form
        viewModel.uiItem.count = "inv"
        viewModel.onTextChanged()
        assertFalse(viewModel.enableSaveButton.value!!)

        //valid form with changed data
        viewModel.uiItem.count = "122"
        viewModel.onTextChanged()
        assertTrue(viewModel.enableSaveButton.value!!)
    }

    @Test
    fun onDeleteItemClicked() {
        //item to edit
        val uiItem = initEditMode()

        viewModel.onTextChanged()

        runTest {
            viewModel.onDeleteItemClicked()
            verify(repository).delete(
                ItemMapper.toItem(uiItem)!!
            )
            //no error event
            assertNull(viewModel.errorEvent.value)
            //navigate back event emitted
            assertTrue(viewModel.navigation.value!!.getContentIfNotHandled() is NavigationCommand.Back)
        }
    }

    @Test
    fun onSaveClicked_happy_scenario_edit_mode() {
        //item to edit
        initEditMode()
        //change a value
        viewModel.uiItem.name = "1aa"
        viewModel.onTextChanged()

        assertTrue(viewModel.enableSaveButton.value!!)
        runTest {
            viewModel.onSaveClicked()
            verify(repository).update(
                Item(
                    "id",
                    "1aa",
                    "",
                    123,
                    DateUtils.dateStringToTimestamp("20/08/2022")!!
                )
            )
            //no error event
            assertNull(viewModel.errorEvent.value)
            //navigate back event emitted
            assertTrue(viewModel.navigation.value!!.getContentIfNotHandled() is NavigationCommand.Back)
        }
    }

    @Test
    fun onSaveClicked_happy_scenario_add_mode() {
        //happy scenario
        enterValidFields()
        assertTrue(viewModel.enableSaveButton.value!!)
        runTest {
            viewModel.onSaveClicked()
            verify(repository).insert(
                Item(
                    "id",
                    "name",
                    "",
                    123,
                    DateUtils.dateStringToTimestamp("20/08/2022")!!
                )
            )
            //no error event
            assertNull(viewModel.errorEvent.value)
            //navigate back event emitted
            assertTrue(viewModel.navigation.value!!.getContentIfNotHandled() is NavigationCommand.Back)
        }
    }

    @Test
    fun onSaveClicked_primary_key_exists_add_mode() {
        val repository = mock<ItemsRepository> {
            onBlocking { insert(any()) }.thenThrow(SQLiteConstraintException("primary key exists"))
        }
        val viewModel = AddItemViewModel(repository)

        enterValidFields(viewModel)
        assertTrue(viewModel.enableSaveButton.value!!)

        runTest {
            viewModel.onSaveClicked()
            verify(repository).insert(
                Item(
                    "id",
                    "name",
                    "",
                    123,
                    DateUtils.dateStringToTimestamp("20/08/2022")!!
                )
            )
            //no navigation event
            assertNull(viewModel.navigation.value)
            //error event emitted
            assertEquals(
                R.string.error_id_already_exists,
                viewModel.errorEvent.value!!.getContentIfNotHandled()
            )
        }
    }

    private fun enterValidFields(viewModel: AddItemViewModel = this.viewModel) {
        viewModel.uiItem.run {
            id = "id"
            name = "name"
            count = "123"
            expiryDate = "20/08/2022"
        }
        viewModel.onTextChanged()
    }

    private fun initEditMode() :UiItem {
        val itemToEdit = UiItem(
            id = "id",
            name = "name",
            count = "123",
            expiryDate = "20/08/2022"
        )
        viewModel.setItemToEdit(itemToEdit)
        return itemToEdit
    }
}