package com.petraride.testapp.ui.listitems

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.petraride.testapp.db.entity.Item
import com.petraride.testapp.nav.NavigationCommand
import com.petraride.testapp.repository.ItemsRepository
import com.petraride.testapp.ui.entity.ItemMapper
import com.petraride.testapp.ui.entity.UiItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineScheduler
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.*

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class ListItemsViewModelTest {

    private lateinit var repository: ItemsRepository
    private lateinit var viewModel: ListItemsViewModel

    @get:Rule
    var instantExecutorRule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(UnconfinedTestDispatcher(TestCoroutineScheduler()))
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun testInitWithNoItems() {
        initViewModel(0)
        verifyAddClick()
    }

    @Test
    fun testInitWithItems() {
        initViewModel(10)
        assertNull(viewModel.navigation.value)
    }

    @Test
    fun getItems_non_empty() {
        val item = Item("id", "name", "desc", 11, 123L)
        initViewModel(itemList = listOf(item))
        viewModel.items.observeForever { }
        viewModel.showItems.observeForever { }
        assertEquals(1, viewModel.items.value!!.size)
        assertEquals(ItemMapper.toUiItem(item), viewModel.items.value!![0])
        assertTrue(viewModel.showItems.value!!)
    }

    @Test
    fun getItems_empty() {
        initViewModel()
        viewModel.items.observeForever { }
        viewModel.showItems.observeForever { }
        assertEquals(0, viewModel.items.value!!.size)
        assertFalse(viewModel.showItems.value!!)
    }

    @Test
    fun onAddClicked() {
        initViewModel()
        viewModel.onAddClicked()
        verifyAddClick()
    }

    @Test
    fun onItemClicked() {
        val uiItem = UiItem()
        initViewModel()
        viewModel.onItemClicked(uiItem)
        assertEquals(
            NavigationCommand.ToDirection(
                ListItemsFragmentDirections.actionListItemsFragmentToAddItemFragment(
                    uiItem
                )
            ),
            viewModel.navigation.value!!.getContentIfNotHandled()
        )
    }

    private fun initViewModel(itemsCount: Int = 10, itemList: List<Item> = listOf()) {
        runBlocking {
            repository = mock {
                onBlocking { getItemsCount() }.thenReturn(itemsCount)
            }
            `when`(repository.getAllItems()).thenReturn(
                flowOf(
                    itemList
                )
            )
            viewModel = ListItemsViewModel(repository)
        }
    }

    private fun verifyAddClick() {
        assertEquals(
            NavigationCommand.ToDirection(
                ListItemsFragmentDirections.actionListItemsFragmentToAddItemFragment(
                    null
                )
            ),
            viewModel.navigation.value!!.getContentIfNotHandled()
        )
    }
}