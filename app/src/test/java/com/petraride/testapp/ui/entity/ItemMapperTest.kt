package com.petraride.testapp.ui.entity

import com.petraride.testapp.db.entity.Item
import com.petraride.testapp.util.DateUtils
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ItemMapperTest {

    @Before
    fun setUp() {
    }

    @Test
    fun toUiItem() {
        //given
        val testItem = Item("id","name","desc",123,0L)
        //when
        val uiItem = ItemMapper.toUiItem(testItem)
        //then
        assertEquals(testItem.id,uiItem.id)
        assertEquals(testItem.name,uiItem.name)
        assertEquals(testItem.description,uiItem.description)
        assertEquals(testItem.count.toString(),uiItem.count)
        assertEquals(DateUtils.timeStampToDateString(testItem.expiryDate),uiItem.expiryDate)
        assertTrue(uiItem.isExpired)
    }

    @Test
    fun toItem() {
        //Blank ID
        val uiItem1 = UiItem(" ", "name", "desc", "0", "01/01/1970")
        assertNull(ItemMapper.toItem(uiItem1))
        //Blank Name
        val uiItem2 = UiItem("1", "    ", "desc", "0", "01/01/1970")
        assertNull(ItemMapper.toItem(uiItem2))
        //Unparsable count
        val uiItem3 = UiItem("1", "name", "desc", "aaa", "01/01/1970")
        assertNull(ItemMapper.toItem(uiItem3))
        //Unparsable date
        val uiItem4 = UiItem("1", "name", "desc", "12", "01/01s/1970")
        assertNull(ItemMapper.toItem(uiItem4))
        //Happy scenario
        val uiItem5 = UiItem("1a", "name", "", "12", "01/01/1970")
        val happyItem = ItemMapper.toItem(uiItem5)
        assertNotNull(happyItem)
        assertEquals(uiItem5.id, happyItem!!.id)
        assertEquals(uiItem5.name, happyItem.name)
        assertEquals(uiItem5.description, happyItem.description)
        assertEquals(DateUtils.dateStringToTimestamp(uiItem5.expiryDate), happyItem.expiryDate)
        assertEquals(uiItem5.count.toInt(), happyItem.count)
    }
}