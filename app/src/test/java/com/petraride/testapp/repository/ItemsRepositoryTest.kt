package com.petraride.testapp.repository

import com.petraride.testapp.db.dao.ItemDao
import com.petraride.testapp.db.entity.Item
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions
import org.mockito.junit.MockitoJUnitRunner

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
internal class ItemsRepositoryTest {

    @Mock
    private lateinit var itemDao: ItemDao
    private lateinit var repository: ItemsRepositoryImpl

    @Before
    fun setUp() {
        repository = ItemsRepositoryImpl(itemDao)
    }

    @Test
    fun getAllItems() {
        repository.getAllItems()
        verify(itemDao).getItems()
        verifyNoMoreInteractions(itemDao)
    }

    @Test
    fun getItemsCount() {
        runTest {
            repository.getItemsCount()
            verify(itemDao).getItemsCount()
            verifyNoMoreInteractions(itemDao)
        }
    }

    @Test
    fun insert() {
        runTest {
            val testItem = Item("test", "test", "test", 0, 0L)
            repository.insert(testItem)
            verify(itemDao).insert(testItem)
            verifyNoMoreInteractions(itemDao)
        }
    }

    @Test
    fun delete() {
        runTest {
            val testItem = Item("test", "test", "test", 0, 0L)
            repository.delete(testItem)
            verify(itemDao).delete(testItem)
            verifyNoMoreInteractions(itemDao)
        }
    }
}