package com.petraride.testapp.util

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("isViewVisible")
fun isViewVisible(view: View, isVisible: Boolean) {
    view.visibility = if (isVisible) View.VISIBLE else View.GONE
}