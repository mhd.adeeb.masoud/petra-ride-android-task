package com.petraride.testapp.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    const val DATE_FORMAT = "dd/MM/yyy"
    private val simpleDateFormat: SimpleDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.UK)

    fun timeStampToDateString(timestamp: Long): String {
        return simpleDateFormat.format(Date(timestamp))
    }

    fun dateStringToTimestamp(date: String): Long? {
        return try {
            simpleDateFormat.parse(date)?.time
        } catch (exp: ParseException) {
            null
        }
    }
}