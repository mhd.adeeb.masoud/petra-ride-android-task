package com.petraride.testapp.db.dao

import androidx.room.*
import com.petraride.testapp.db.entity.Item
import kotlinx.coroutines.flow.Flow

@Dao
interface ItemDao {
    @Query("SELECT * FROM Item ORDER BY item_name ASC")
    fun getItems(): Flow<List<Item>>

    @Query("SELECT COUNT(item_id) from Item")
    suspend fun getItemsCount() : Int

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(item: Item)

    @Update
    suspend fun update(item: Item)

    @Delete
    suspend fun delete(item: Item)
}