package com.petraride.testapp.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.petraride.testapp.db.dao.ItemDao
import com.petraride.testapp.db.entity.Item

@Database(entities = [Item::class], version = 1, exportSchema = false)
abstract class StockManagementDB: RoomDatabase() {
    abstract fun itemDao(): ItemDao
}