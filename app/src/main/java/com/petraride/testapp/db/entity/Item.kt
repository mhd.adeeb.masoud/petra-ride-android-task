package com.petraride.testapp.db.entity

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.ColumnInfo.INTEGER
import androidx.room.ColumnInfo.TEXT
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Item")
data class Item(
    @PrimaryKey @ColumnInfo(name = "item_id", typeAffinity = TEXT)
    val id: String,
    @ColumnInfo(name = "item_name", typeAffinity = TEXT)
    @NonNull
    val name: String,
    @ColumnInfo(name = "item_description", typeAffinity = TEXT, defaultValue = "''")
    @NonNull
    val description: String,
    @ColumnInfo(name = "item_count", typeAffinity = INTEGER, defaultValue = "0")
    @NonNull
    val count: Int,
    @ColumnInfo(name = "item_expiry_date")
    @NonNull
    val expiryDate: Long

)