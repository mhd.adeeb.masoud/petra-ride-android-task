package com.petraride.testapp.repository

import androidx.annotation.WorkerThread
import com.petraride.testapp.db.dao.ItemDao
import com.petraride.testapp.db.entity.Item
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

interface ItemsRepository {
    fun getAllItems(): Flow<List<Item>>
    suspend fun getItemsCount(): Int
    suspend fun insert(item: Item)
    suspend fun update(item: Item)
    suspend fun delete(item: Item)
}

class ItemsRepositoryImpl @Inject constructor(private val itemDao: ItemDao) : ItemsRepository {

    override fun getAllItems() = itemDao.getItems()

    @WorkerThread
    override suspend fun getItemsCount(): Int = itemDao.getItemsCount()

    @WorkerThread
    override suspend fun insert(item: Item) = itemDao.insert(item)

    @WorkerThread
    override suspend fun update(item: Item) = itemDao.update(item)

    @WorkerThread
    override suspend fun delete(item: Item) = itemDao.delete(item)
}