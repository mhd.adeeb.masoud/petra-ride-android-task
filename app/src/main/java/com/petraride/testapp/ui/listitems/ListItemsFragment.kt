package com.petraride.testapp.ui.listitems

import android.os.Bundle
import android.view.ContextMenu
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.petraride.testapp.R
import com.petraride.testapp.databinding.FragmentListItemsBinding
import com.petraride.testapp.db.entity.Item
import com.petraride.testapp.ui.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ListItemsFragment : BaseFragment<FragmentListItemsBinding, ListItemsViewModel>() {

    private val recyclerAdapter = ItemsRecyclerAdapter()
    override val viewModel: ListItemsViewModel by viewModels()
    override val layoutId = R.layout.fragment_list_items


    override fun onReady(savedInstanceState: Bundle?) {
        setupRecyclerView()
        recyclerAdapter.itemClickListener = viewModel::onItemClicked
        viewModel.items.observe(viewLifecycleOwner) {
            recyclerAdapter.updateList(it)
        }
    }

    private fun setupRecyclerView() {
        binding.recyclerItems.setHasFixedSize(true)
        binding.recyclerItems.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerItems.adapter = recyclerAdapter
    }

}