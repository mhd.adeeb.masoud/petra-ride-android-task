package com.petraride.testapp.ui.listitems

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.petraride.testapp.R
import com.petraride.testapp.databinding.ItemRowBinding
import com.petraride.testapp.ui.entity.UiItem

class ItemsRecyclerAdapter : RecyclerView.Adapter<ItemsRecyclerAdapter.ViewHolder>() {

    private val itemsList = mutableListOf<UiItem>()
    var itemClickListener: ((UiItem) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemRowBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_row, parent, false
        )
        return ViewHolder(binding)
    }

    fun updateList(newList: List<UiItem>) {
        val diffUtilCallBack = DiffUtilCallback(itemsList, newList)
        val diffResult = DiffUtil.calculateDiff(diffUtilCallBack)
        itemsList.clear()
        itemsList.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemsList[position])
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }


    inner class ViewHolder(var binding: ItemRowBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UiItem) {
            binding.root.setOnClickListener { itemClickListener?.invoke(item) }
            binding.item = item
            binding.executePendingBindings()
            binding.root.setBackgroundColor(
                if (item.isExpired) ContextCompat.getColor(
                    binding.root.context,
                    R.color.color_expired_bg
                ) else Color.WHITE
            )
        }
    }

    private class DiffUtilCallback(
        private val oldList: List<UiItem>,
        private val newList: List<UiItem>
    ) :
        DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].id == newList[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]
    }
}