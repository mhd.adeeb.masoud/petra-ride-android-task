package com.petraride.testapp.ui.additem

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import com.petraride.testapp.R
import com.petraride.testapp.databinding.FragmentAddItemBinding
import com.petraride.testapp.ui.base.BaseFragment
import com.petraride.testapp.util.DateUtils
import com.petraride.testapp.util.observeNonNull
import com.petraride.testapp.util.transformIntoDatePicker
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddItemFragment : BaseFragment<FragmentAddItemBinding, AddItemViewModel>() {
    override val layoutId: Int = R.layout.fragment_add_item
    override val viewModel: AddItemViewModel by viewModels()

    override fun onReady(savedInstanceState: Bundle?) {
        binding.etExpiry.transformIntoDatePicker(DateUtils.DATE_FORMAT)

        viewModel.setItemToEdit(arguments?.getParcelable("itemToEdit"))

        viewModel.errorEvent.observeNonNull(viewLifecycleOwner) {
            it.getContentIfNotHandled()?.let { messageId ->
                showErrorDialog(messageId)
            }
        }
        viewModel.appBarTitleResource.observeNonNull(viewLifecycleOwner) {
            (requireActivity() as AppCompatActivity).supportActionBar?.title = getString(it)
        }
        if (viewModel.showDeleteButton)
            setupOptionsMenu()
    }

    private fun setupOptionsMenu() {
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.edit_fragment_menu, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return if (menuItem.itemId == R.id.menu_item_delete) {
                    viewModel.onDeleteItemClicked()
                    true
                } else {
                    false
                }
            }

        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

}