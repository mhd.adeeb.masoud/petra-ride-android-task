package com.petraride.testapp.ui.additem

import android.database.sqlite.SQLiteConstraintException
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.petraride.testapp.R
import com.petraride.testapp.db.entity.Item
import com.petraride.testapp.repository.ItemsRepository
import com.petraride.testapp.ui.base.BaseViewModel
import com.petraride.testapp.ui.entity.ItemMapper
import com.petraride.testapp.ui.entity.UiItem
import com.petraride.testapp.util.Event
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddItemViewModel @Inject constructor(private val itemsRepository: ItemsRepository) :
    BaseViewModel() {

    val uiItem = UiItem()
    private var itemToEdit: UiItem? = null
    private var itemToAdd: Item? = null

    private val _enableSaveButton = MutableLiveData(false)
    val enableSaveButton: LiveData<Boolean> = _enableSaveButton

    private val _enableIdField = MutableLiveData(true)
    val enableIdField: LiveData<Boolean> = _enableIdField

    private var _showDeleteButton = false
    val showDeleteButton: Boolean
        get() {
            return _showDeleteButton
        }

    private val _appBarTitleResource = MutableLiveData(R.string.add_item_title)
    val appBarTitleResource: LiveData<Int> = _appBarTitleResource

    private val _errorEvent = MutableLiveData<Event<Int>>()
    val errorEvent: LiveData<Event<Int>> = _errorEvent

    fun onTextChanged() {
        itemToAdd = ItemMapper.toItem(uiItem)
        _enableSaveButton.value = itemToAdd != null && (itemToEdit == null || itemToEdit != uiItem)
    }

    fun onSaveClicked() {
        itemToAdd?.let {
            viewModelScope.launch {
                try {
                    if (itemToEdit == null) {
                        itemsRepository.insert(it)
                    } else {
                        itemsRepository.update(it)
                    }
                    navigateBack()
                } catch (exception: SQLiteConstraintException) {
                    _errorEvent.value = Event(R.string.error_id_already_exists)
                }
            }
        }
    }

    fun setItemToEdit(uiItem: UiItem?) {
        uiItem?.let {
            itemToEdit = uiItem
            this.uiItem.run {
                id = it.id
                name = it.name
                description = it.description
                count = it.count
                expiryDate = it.expiryDate
                isExpired = it.isExpired
            }
            _enableIdField.value = false
            _appBarTitleResource.value = R.string.edit_item_title
            _showDeleteButton = true
        }
    }

    fun onDeleteItemClicked() {
        itemToEdit?.let {
            viewModelScope.launch {
                itemsRepository.delete(ItemMapper.toItem(it)!!)
                navigateBack()
            }
        }
    }

}