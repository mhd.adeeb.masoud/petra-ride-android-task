package com.petraride.testapp.ui.entity

import android.os.Parcel
import android.os.Parcelable
import com.petraride.testapp.db.entity.Item
import com.petraride.testapp.util.DateUtils
import java.util.*

data class UiItem(
    var id: String = "",
    var name: String = "",
    var description: String = "",
    var count: String = "",
    var expiryDate: String = "",
    var isExpired: Boolean = false
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString().orEmpty(),
        parcel.readString().orEmpty(),
        parcel.readString().orEmpty(),
        parcel.readString().orEmpty(),
        parcel.readString().orEmpty(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(count)
        parcel.writeString(expiryDate)
        parcel.writeByte(if (isExpired) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UiItem> {
        override fun createFromParcel(parcel: Parcel): UiItem {
            return UiItem(parcel)
        }

        override fun newArray(size: Int): Array<UiItem?> {
            return arrayOfNulls(size)
        }
    }
}

object ItemMapper {
    fun toUiItem(item: Item) = UiItem(
        item.id,
        item.name,
        item.description,
        item.count.toString(),
        DateUtils.timeStampToDateString(item.expiryDate),
        item.expiryDate < Date().time
    )

    fun toItem(uiItem: UiItem): Item? {
        if (uiItem.name.isBlank() || uiItem.id.isBlank()) return null
        val expTimestamp = DateUtils.dateStringToTimestamp(uiItem.expiryDate) ?: return null
        val count = uiItem.count.toIntOrNull() ?: return null
        return Item(
            uiItem.id,
            uiItem.name,
            uiItem.description,
            count,
            expTimestamp
        )
    }
}