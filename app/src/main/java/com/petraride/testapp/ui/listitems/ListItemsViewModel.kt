package com.petraride.testapp.ui.listitems

import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.petraride.testapp.repository.ItemsRepository
import com.petraride.testapp.ui.base.BaseViewModel
import com.petraride.testapp.ui.entity.ItemMapper
import com.petraride.testapp.ui.entity.UiItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListItemsViewModel @Inject constructor(private val itemsRepository: ItemsRepository) :
    BaseViewModel() {

    init {
        goToAddWhenEmpty()
    }

    val items =
        itemsRepository.getAllItems().map { list -> list.map { ItemMapper.toUiItem(it) } }
            .asLiveData()

    val showItems: LiveData<Boolean> = items.map {
        it.isNotEmpty()
    }

    fun onAddClicked() {
        navigate(ListItemsFragmentDirections.actionListItemsFragmentToAddItemFragment(null))
    }

    fun onItemClicked(uiItem: UiItem) {
        navigate(ListItemsFragmentDirections.actionListItemsFragmentToAddItemFragment(uiItem))
    }

    private fun goToAddWhenEmpty() {
        viewModelScope.launch {
            if (itemsRepository.getItemsCount() == 0)
                onAddClicked()
        }
    }
}