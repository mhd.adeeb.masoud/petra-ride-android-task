package com.petraride.testapp.di

import android.content.Context
import androidx.room.Room
import com.petraride.testapp.db.StockManagementDB
import com.petraride.testapp.db.dao.ItemDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @JvmStatic
    fun provideItemDao(db: StockManagementDB) = db.itemDao()

    @Provides
    @JvmStatic
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): StockManagementDB {
        return Room.databaseBuilder(
            appContext,
            StockManagementDB::class.java,
            "StockManagementDB"
        ).build()
    }
}