package com.petraride.testapp.di

import com.petraride.testapp.db.dao.ItemDao
import com.petraride.testapp.repository.ItemsRepository
import com.petraride.testapp.repository.ItemsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Provides
    @JvmStatic
    fun provideItemsRepository(itemDao: ItemDao): ItemsRepository = ItemsRepositoryImpl(itemDao)
}