package com.petraride.testapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PetraTestApplication: Application() {
}